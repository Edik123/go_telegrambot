package main

import (
	"log"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"bitbucket.org/Edik123/go_telegrambot/src/master/config"
	"bitbucket.org/Edik123/go_telegrambot/src/master/models/pocket"
	"bitbucket.org/Edik123/go_telegrambot/src/master/models/telegram"
)

func main(){
	cfg, err := config.Init()
	if err != nil {
		log.Fatal(err)
	}

	botapi, err := tgbotapi.NewBotAPI(cfg.TelegramToken)
	if err != nil {
		log.Panic(err)
	}

	botapi.Debug = true  // console logs

	log.Printf("Authorized on account %s", botapi.Self.UserName)

	pocketClient, err := pocket.NewClient(cfg.PocketConsumerKey)

	if err != nil {
		log.Fatal(err)
	}

	bot := telegram.NewBot(botapi, pocketClient, cfg.RedirectURL)

	if err := bot.Start(); err != nil {
		log.Fatal(err)
	}
}