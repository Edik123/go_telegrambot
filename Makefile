.PHONY:

build:
	go build -o ./.bin/bot main.go

run: build
	./.bin/bot

docker-build:
	docker build -t telegrambot .

docker-start:
	docker run --env-file .env -p 80:80 telegrambot