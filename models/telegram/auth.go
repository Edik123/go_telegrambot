package telegram

import (
	"fmt"
	"context"
)

func (b *Bot) createAuthorizationLink(chatID int64) (string, error) {
	redirectUrl := b.generateRedirectURL(chatID)

	token, err := b.client.GetRequestToken(context.Background(), redirectUrl)
	if err != nil {
		return "", err
	}

	return b.client.GetAuthorizationURL(token, redirectUrl)
}

func (b *Bot) generateRedirectURL(chatID int64) string {
	return fmt.Sprintf("%s?chat_id=%d", b.redirectURL, chatID)
}