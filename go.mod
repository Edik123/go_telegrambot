module bitbucket.org/Edik123/go_telegrambot/src/master

go 1.16

require (
	github.com/go-telegram-bot-api/telegram-bot-api v4.6.4+incompatible // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/spf13/viper v1.8.1 // indirect
	github.com/technoweenie/multipartstreamer v1.0.1 // indirect
)
