package config

import (
	"os"
	"github.com/spf13/viper"
)

type Config struct {
	TelegramToken     string
	PocketConsumerKey string
	RedirectURL     string
}

func Init() (*Config, error) {
	var cfg Config

	if err := fromEnv(&cfg); err != nil {
		return nil, err
	}

	return &cfg, nil
}

func fromEnv(cfg *Config) error {
	os.Setenv("TOKEN", "1884448184:AAG_JqTOpIQUKZ_j7q2b0_mt-3KYzy5dCUk")
	os.Setenv("CONSUMER_KEY", "97909-6c8f89862fde05633691f55d")
	os.Setenv("REDIRECT_URL", "http://localhost/")

	if err := viper.BindEnv("token"); err != nil {
		return err
	}
	cfg.TelegramToken = viper.GetString("token")

	if err := viper.BindEnv("consumer_key"); err != nil {
		return err
	}
	cfg.PocketConsumerKey = viper.GetString("consumer_key")

	if err := viper.BindEnv("redirect_url"); err != nil {
		return err
	}
	cfg.RedirectURL = viper.GetString("redirect_url")

	return nil
}