FROM golang:1.16-alpine3.14 AS builder

RUN go version

COPY . /bitbucket.org/Edik123/go_telegrambot/src/master/
WORKDIR /bitbucket.org/Edik123/go_telegrambot/src/master/

RUN go mod download
RUN GOOS=linux go build -o ./.bin/bot ./main.go

FROM alpine:latest

WORKDIR /root/

COPY --from=0 /bitbucket.org/Edik123/go_telegrambot/src/master/.bin/bot .
COPY --from=0 /bitbucket.org/Edik123/go_telegrambot/src/master/config config/

EXPOSE 80

CMD ["./bot"]